# Helsing Software testing workshop -- Python exercise

Python exercises for the [Software Testing workshop](https://gitlab.com/helsing/software-testing-workshop).

### Instructions for workshop participants

This repository contains a simple script for rendering a medal table for the Olympic games. It fetches a list of medals from the olympics.com web service, computes the medal table, and renders the result to STDOUT.

Your task is to:
1. Refactor the code base to make it testable
1. Write unit or integration tests for the components you have identified

Hints:
- It's totally OK (desired, actually) to overengineer the decomp a bit. Yes, this is merely a silly 100 line script, but for the sake of the exercise, please imagine it were a production code base and your task is to improve it.
- Start by identifying different components, for example:
  - a component for fetching and parsing the input data
  - a component for computing the medal table
  - a component for maintaining the top-n countries
  - a component for displaying/rendering the top-n countries
  - a component for scheduling a regular refresh of the medal table
- Encapsulate these components in suitable classes, maybe even introduce interfaces if you like
- Write unit tests for each component
- Write an integration test for the entire application

### Development environment

This repository uses a standard [Poetry](https://python-poetry.org) build
setup. Run `poetry shell` to get a shell with all dependencies installed.

### Contributions

Contributions are welcome. Please create a GitLab merge request and tag one of the repository owners.

### License
This work is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](https://creativecommons.org/licenses/by-sa/4.0/).
