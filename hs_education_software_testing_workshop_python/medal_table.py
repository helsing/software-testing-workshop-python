import logging
from collections import defaultdict
from dataclasses import dataclass
from enum import Enum
from time import sleep

import requests

logging.basicConfig(level=logging.INFO)


@dataclass
class Medal:
    class Type(Enum):
        GOLD = 1
        SILVER = 2
        BRONZE = 3

    type: Type
    country: str


def fetch_medals() -> list[Medal]:
    """
    Determines the current list of (athletics) medals as advertised by olympics.com.
    """
    logging.info("Fetching medal table")

    # From: https://olympics.com/en/olympic-games/tokyo-2020/results/athletics
    json = requests.get("https://helsing.gitlab.io/software-testing-workshop-python/athletics.json").json()
    medals = []
    for event in json["pageProps"]["gameDiscipline"]["events"]:
        for award in event["awards"]:
            type = Medal.Type[award["medalType"]]
            if not award["participant"].get("countryObject"):
                country = award["participant"]["title"]
            else:
                country = award["participant"]["countryObject"]["name"]
            medal = Medal(type, country)
            medals.append(medal)
    return medals


def create_table(medals: list[Medal]) -> list[tuple[str, int, int, int]]:
    """
    Returns a list of (country, #gold, #silver, #bronze) tuples
    ranked by medal count.
    """
    by_country: dict[str, list[Medal]] = defaultdict(list)
    for medal in medals:
        by_country[medal.country].append(medal)
    countries = []
    for country, country_medals in by_country.items():
        countries.append(
            (
                country,
                len([medal for medal in country_medals if medal.type == Medal.Type.GOLD]),
                len([medal for medal in country_medals if medal.type == Medal.Type.SILVER]),
                len([medal for medal in country_medals if medal.type == Medal.Type.BRONZE]),
            )
        )
    countries.sort(key=lambda x: (x[1], x[2], x[3]), reverse=True)
    return countries


def main() -> None:
    """
    Continuously polls the olympics.com web service to retrieve a list of medals
    and prints the top-5 countries of the medal ranking (whenever it changes).
    """
    last_top5 = None

    while True:
        medals = fetch_medals()
        table = create_table(medals)[0:5]
        top5 = [tuple[0] for tuple in table]
        if top5 != last_top5:
            print(top5)
        last_top5 = top5
        sleep(2)


if __name__ == "__main__":
    main()
